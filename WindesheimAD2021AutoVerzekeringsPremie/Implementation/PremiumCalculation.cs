﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("WindesheimAD2021AutoVerzekeringsPremie.Tests")]
namespace WindesheimAD2021AutoVerzekeringsPremie.Implementation
{
    internal class PremiumCalculation
    {
        public double PremiumAmountPerYear { get; private set; }
        private readonly int PRECISION = 2;

        internal enum PaymentPeriod
        {
            YEAR,
            MONTH
        }
                
        internal PremiumCalculation(Vehicle vehicle, PolicyHolder policyHolder, InsuranceCoverage coverage)
        {
            
            double premium = 0d;
            premium += CalculateBasePremium(vehicle);
            
            // Zou alleen < 5 jaar moeten zijn en niet <= 5 jaar!
            if(policyHolder.Age < 23 || policyHolder.LicenseAge < 5)
            {
                premium *= 1.15;
            }

            premium = UpdatePremiumForPostalCode(premium, policyHolder.PostalCode);

            if(coverage == InsuranceCoverage.WA_PLUS)
            {
                premium *= 1.2;
            } else if (coverage == InsuranceCoverage.ALL_RISK)
            {
                premium *= 2;
            }

            premium = UpdatePremiumForNoClaimYears(premium, policyHolder.NoClaimYears);

            // Premium zou naar 2 decimalen moeten afronden jij minkukel.
            PremiumAmountPerYear = Math.Round(premium, PRECISION);
        }

        private static double UpdatePremiumForNoClaimYears(double premium, int years)
        {
            // Int moet een double zijn, Anders is het nogal lastig rekenenen met decimalen :P
            double NoClaimPercentage = (years - 5) * 5;
            if (NoClaimPercentage >= 65) { NoClaimPercentage = 65; }
            if (NoClaimPercentage <= 0) { NoClaimPercentage = 0; }
            return premium * ((100 - NoClaimPercentage) / 100);
        }

        private static double UpdatePremiumForPostalCode(double premium, int postalCode) => postalCode switch
        {
            >= 1000 and < 3600 => premium * 1.05,
            < 4500 => premium * 1.02,
            _ => premium,
        };

        internal double PremiumPaymentAmount(PaymentPeriod period)
        {
            double result = period == PaymentPeriod.YEAR ? PremiumAmountPerYear / 1.025 : PremiumAmountPerYear / 12;
            return Math.Round(result, PRECISION);
        }

        internal static double CalculateBasePremium(Vehicle vehicle)
        {
            // vehicle.ValueInEuros / 100 - vehicle.Age + vehicle.PowerInKw / 5 / 3, eerst de som dan pas door 3 delen :P
            // Rekenvolgorde and what.
            return (vehicle.ValueInEuros / 100 - vehicle.Age + vehicle.PowerInKw / 5) / 3;
        }
    }
}
