using Xunit;
using static WindesheimAD2021AutoVerzekeringsPremie.Implementation.PremiumCalculation;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;

namespace WindesheimAD2021AutoVerzekeringsPremie.Tests
{
    public class WindesheimAD2021AutoVerzekeringsPremieTests
    {
        [Fact]
        public void Test_IsCalculateBasePremiumCorrect()
        {
            // Arrange
            Vehicle Car = new Vehicle(275, 750, 2016);
            // Act
            double ActualCalculation = CalculateBasePremium(Car);
            // Assert
            Assert.Equal(19, ActualCalculation);
        }

        [Fact]
        public void Test_PremiumGetsRounded()
        {
            Vehicle Car = new Vehicle(275, 750, 2016);
            PolicyHolder Jerones = new PolicyHolder(24, "25-04-2016", 1333, 0);
            PremiumCalculation premiumCalculation = new(Car, Jerones, InsuranceCoverage.WA);

            Assert.Equal(19.95, premiumCalculation.PremiumAmountPerYear);
        }

        [Theory]
        // jonger dan 23, langer dan 5 jaar.
        [InlineData(22, "25-04-2000", 22.38)]
        // jonger dan 23, korter dan 5 jaar.
        [InlineData(22, "25-04-2019", 22.38)]
        // jonger dan 23, exact 5 jaar.
        [InlineData(22, "25-04-2016", 22.38)]

        // Exact 23, langer dan 5 jaar.
        [InlineData(23, "25-04-2000", 19.46)]
        // Exact 23, korter dan 5 jaar.
        [InlineData(23, "25-04-2019", 22.38)]
        // Exact 23, exact 5 jaar.
        [InlineData(23, "25-04-2016", 19.46)]

        // Ouder dan 23, langer dan 5 jaar.
        [InlineData(24, "25-04-2000", 19.46)]
        // Ouder dan 23, korter dan 5 jaar.
        [InlineData(24, "25-04-2019", 22.38)]
        // Ouder dan 23, exact 5 jaar.
        [InlineData(24, "25-04-2016", 19.46)]

        // Toekomst
        [InlineData(24, "25-04-4000", 22.38)]
        public void Test_DoesPremiumApplyCorrectPercentages(int Age, string DriverlicenseStartDate, double expectedValue)
        {
            Vehicle Car = new Vehicle(275, 750, 2016);
            PolicyHolder Jerones = new PolicyHolder(Age, DriverlicenseStartDate, 1333, 0);
            PremiumCalculation premiumCalculation = new(Car, Jerones, InsuranceCoverage.WA);

            double ActualCalculation = premiumCalculation.PremiumPaymentAmount(PaymentPeriod.YEAR);

            Assert.Equal(expectedValue, ActualCalculation);
        }

        [Theory]
        // Exact 1000
        [InlineData(1000, 22.38)]
        // Tussen 1000 - 3599
        [InlineData(1333, 22.38)]
        // Exact 3599
        [InlineData(3599, 22.38)]
        // Exact 3600
        [InlineData(3600, 21.75)]
        // Tussen 3600 - 4499 
        [InlineData(4300, 21.75)]
        // Exact 4499 
        [InlineData(4499, 21.75)]
        // Higher then 4499
        [InlineData(4500, 21.32)]
        public void Test_CheckPostCodePercentages(int postcode, double expectedValue)
        {
            Vehicle Car = new Vehicle(275, 750, 2016);
            PolicyHolder Jerones = new PolicyHolder(24, "25-04-2019", postcode, 0);
            PremiumCalculation premiumCalculation = new(Car, Jerones, InsuranceCoverage.WA);

            double ActualCalculation = premiumCalculation.PremiumPaymentAmount(PaymentPeriod.YEAR);

            Assert.Equal(expectedValue, ActualCalculation);
        }

        [Fact]
        public void Test_YearSubscriptionGivesTwoAndAHalfPercentDiscount()
        {
            Vehicle Car = new Vehicle(275, 750, 2016);
            PolicyHolder Jerones = new PolicyHolder(24, "25-04-2019", 1333, 0);
            PremiumCalculation premiumCalculation = new(Car, Jerones, InsuranceCoverage.WA);

            double YearPremium = premiumCalculation.PremiumPaymentAmount(PaymentPeriod.YEAR);
            double MonthPremium = premiumCalculation.PremiumPaymentAmount(PaymentPeriod.MONTH);

            double YearMonthPremium = (YearPremium * 1.025) / 12;

            Assert.Equal(MonthPremium, YearMonthPremium, 2);
        }

        [Fact]
        public void Test_WAPlusIs20PercentMoreExpensive()
        {
            Vehicle Car = new Vehicle(275, 750, 2016);
            PolicyHolder Jerones = new PolicyHolder(24, "25-04-2016", 1333, 0);

            PremiumCalculation WA = new PremiumCalculation(Car, Jerones, InsuranceCoverage.WA);

            PremiumCalculation WAPLUS = new PremiumCalculation(Car, Jerones, InsuranceCoverage.WA_PLUS);

            double ActualAmount = (WAPLUS.PremiumAmountPerYear / 120) * 100;

            Assert.Equal(WA.PremiumAmountPerYear, ActualAmount, 2);
        }

        [Fact]
        public void Test_AllRiskHellaExpensive()
        {
            Vehicle Car = new Vehicle(275, 750, 2016);
            PolicyHolder Jerones = new PolicyHolder(24, "25-04-2016", 1333, 0);

            PremiumCalculation WA = new PremiumCalculation(Car, Jerones, InsuranceCoverage.WA);
            PremiumCalculation ALLRISK = new PremiumCalculation(Car, Jerones, InsuranceCoverage.ALL_RISK);

            double ActualDividedPremiumByHalf = (ALLRISK.PremiumAmountPerYear / 200) * 100;

            Assert.Equal(WA.PremiumAmountPerYear, ActualDividedPremiumByHalf);
        }

        [Theory]
        [InlineData(5, 22.94)]
        [InlineData(6, 21.8)]
        [InlineData(12, 14.91)]
        [InlineData(18, 8.03)]
        [InlineData(24, 8.03)]

        public void Test_NoClaimYearsGivesDiscount(int FreeYears, double expected)
        {
            Vehicle Car = new Vehicle(275, 750, 2016);
            PolicyHolder Jerones = new PolicyHolder(24, "25-04-2019", 1333, FreeYears);
            PremiumCalculation premiumCalculation = new(Car, Jerones, InsuranceCoverage.WA);

            Assert.Equal(expected, premiumCalculation.PremiumAmountPerYear);
        }

    }
}
