﻿﻿```
  _____          _____   _____ _    _ _____  ______ 
 / ____|   /\   |  __ \ / ____| |  | |  __ \|  ____|
| |       /  \  | |__) | (___ | |  | | |__) | |__   
| |      / /\ \ |  _  / \___ \| |  | |  _  /|  __|  
| |____ / ____ \| | \ \ ____) | |__| | | \ \| |____ 
 \_____/_/    \_\_|  \_\_____/ \____/|_|  \_\______|
```
# Tests
Test loop in dezelfde volgorde als in de tests in Applicatie.
<div>
<h2>Test 1: Is de basis premie calculatie correct?</h2>
<h2>Doel</h2>
<p>Checken of de brekening het juiste resultaat oplevert, De berekening is: <b>(voertuig / 100 - leeftijd + vermogen in KW / 5) / 3</b>.
<p>Was oorsproonkelijk: <b>voertuig / 100 - leeftijd + vermogen in KW / 5 / 3</b>.</p>
<h2>Technieken</h2>
<p>Ik verwacht 1 specifieke uitkomst, dus er kan een fact gebruikt worden, eenmalig goed weet ik dat de berekening klopt.</p>
<h2>Gebruikte data:</h2>
<p>Het is een fact dus ik verwachte maar 1 specifieke uitkomst en dat was <b>19</b>.</p>
<h2>Geteste Factor:</h2>
<p>Is de berekening gelijk aan de verwachte uitkomst.</p>
<h2>Verwachting:</h2>
<p>Aangezien het een test opdracht is, verwachte ik dat de eerste test niet zou moeten voldoen, wat waar blijkt te zijn maar daarover meer in de ondervinding.</p>
<h2>Ondervinding:</h2>
<p>Na de eerste test kwam ik er al achter dat de uitkomst van de som niet gelijk was aan wat ik verwachte via de request in de requirements. De som bleek fout te zijn, Omdat de juiste regels van de rekenvolgorde niet waren toegepast. Oorspronkelijk stonden er geen haken () over het eerste gedeelte van de som, waardoor de volgorde incorrect was.
<b>(voertuig / 100 - leeftijd + vermogen in KW / 5) / 3</b>. Na de berekening aan te hebben gepast klopte mijn verwachting.
</p>
</div>
<hr>
<div>
<h2>Test 2: Wordt de calculatie afgerond op 2 decimalen?</h2>
<h2>Doel</h2>
<p>Eerste test puur getest of de berekening correct was, nu toch even de 2 decimalen los checken</b>.
<h2>Technieken</h2>
<p>Ik verwacht 1 specifieke uitkomst, dus er kan een fact gebruikt worden, eenmalig goed weet ik dat de berekening klopt.</p>
<h2>Gebruikte data:</h2>
<p>Het is een fact dus ik verwachte maar 1 specifieke uitkomst en dat was <b>19.95</b>.</p>
<h2>Geteste Factor:</h2>
<p>Is de berekening gelijk aan de verwachte uitkomst.</p>
<h2>Verwachting:</h2>
<p>Ik verwacht dat er afgerond wordt naar 2 decimalen, maar aangezien het een requirement is zal het wel ergens niet goed gaan.</p>
<h2>Ondervinding:</h2>
<p>
Het bleek inderdaad dat bij de eindsom de <code>PremiumAmountPerYear = Math.Round(premium, PRECISION)</code> oorspronkelijk niet naar 2 decimalen werdt afgerond. En dat een bepaalde variable als Int werd gedeclared inplaats van Double. Oftewel je kan nooit op een decimaal uitkomen. Nadat gefixed te hebben werkten de test naar toebehoren.
</p>
</div>
<hr>
<div>
<h2>Test 3: Krijgt de begruiker correcte permie opslagen?</h2>
<h2>Doel</h2>
<p>Controlleren of de gebruiker opslag krijgt in deze situaties:</p>
<ul>
<li>Jonger is dan 23 jaar oud.</li>
<li>Korter dan 5 jaar zijn rijbewijs in bezit heeft.</li>
</ul>
<p>Deze verwachtingen kunnen over elkaar heen vallen, ouder dan 23 maar toch minder dan 5 jaar rijbewijs is nog steeds opslag :P.</p>
<h2>Technieken</h2>
<p>Voor deze test gebruik ik een theory waar ik al mijn bedachte grenswaarden/equivilentieklassen ga controlleren op hun uitkomsten.</p>
<h2>Gebruikte data:</h2>

    // jonger dan 23, langer dan 5 jaar.
    [InlineData(22, "25-04-2000", 22.38)]
    // jonger dan 23, korter dan 5 jaar.
    [InlineData(22, "25-04-2019", 22.38)]
    // jonger dan 23, exact 5 jaar.
    [InlineData(22, "25-04-2016", 22.38)]

    // Exact 23, langer dan 5 jaar.
    [InlineData(23, "25-04-2000", 19.46)]
    // Exact 23, korter dan 5 jaar.
    [InlineData(23, "25-04-2019", 22.38)]
    // Exact 23, exact 5 jaar.
    [InlineData(23, "25-04-2016", 19.46)]

    // Ouder dan 23, langer dan 5 jaar.
    [InlineData(24, "25-04-2000", 19.46)]
    // Ouder dan 23, korter dan 5 jaar.
    [InlineData(24, "25-04-2019", 22.38)]
    // Ouder dan 23, exact 5 jaar.
    [InlineData(24, "25-04-2016", 19.46)]

    // Toekomst
    [InlineData(24, "25-04-4000", 22.38)]
<h2>Geteste Factor:</h2>
<p>Klopt de toegpaste premie, op basis van de gestelde eisen. Jonger dan 23, of korter dan 5 jaar rijbewijs</p>
<h2>Verwachting:</h2>
<p>Ik verwacht dat de gebruiker een premie opslag van 15% wanneer hij/zij jonger is dan 23 jaar of korter dan vijf jaar zijn rijbewijs heeft.
<h2>Ondervinding:</h2>
<p>
Tijdens het testen kwam ik erachter dat er <code><= 5</code> stond terwijl er in de requirements alleen korter dan 5 stond. Oftewel het moest <code>< 5</code> zijn.
</p>
</div>
<hr>
<div>
<h2>Test 4: Krijg je de juiste risico opslag bij bepaalde postcodes?</h2>
<h2>Doel</h2>
<p>Checken of postcodes 10xx - 35xx: 5% risico opslag krijgen.</p>
<p>Checken of postcodes 36xx - 44xx: 2% risico opslag krijgen.</p>
<h2>Technieken</h2>
<p>Voor deze test gebruik ik een theory waar ik al mijn bedachte grenswaarden/equivilentieklassen ga controlleren op hun uitkomsten.</p>
<h2>Gebruikte data:</h2>

    // Exact 1000
    [InlineData(1000, 22.38)]
    // Tussen 1000 - 3599
    [InlineData(1333, 22.38)]
    // Exact 3599
    [InlineData(3599, 22.38)]
    // Exact 3600
    [InlineData(3600, 21.75)]
    // Tussen 3600 - 4499 
    [InlineData(4300, 21.75)]
    // Exact 4499 
    [InlineData(4499, 21.75)]
    // Higher then 4499
    [InlineData(4500, 21.32)]
<h2>Geteste Factor:</h2>
<p>Krijgen de postcodes hun correcte opslag?</p>
<h2>Verwachting:</h2>
<p>De postcodes krijgen hun correcte opslag.</p>
<h2>Ondervinding:</h2>
<p>
De risico opslag correct toegepast!
</p>
</div>
<hr>
<div>
<h2>Test 5: Geeft een jaar subscriptie 2.5% korting?</h2>
<h2>Doel</h2>
<p>Checken of een jaar subscriptie de 2.5% korting geeft.</b>
<h2>Technieken</h2>
<p>Ik verwacht 1 specifieke uitkomst, dus er kan een fact gebruikt worden, eenmalig goed weet ik dat de berekening klopt.</p>
<h2>Gebruikte data:</h2>
<p>Het is een fact dus ik verwachte maar 1 specifieke uitkomst.</p>
<h2>Geteste Factor:</h2>
<p>Is een jaar abbonement 2.5% goedkoper.</p>
<h2>Verwachting:</h2>
<p>Ik verwacht eventuele problemen met afrondingen.</p>
<h2>Ondervinding:</h2>
<p>
Na het doen van de tests ging het helemaal goed, omdat ik na de vorige test al alle afrondingen had opgelost.
</p>
</div>
<hr>
<h2>Test 6: Is WAPLUS 20% duurder dan normale WA?</h2>
<h2>Doel</h2>
<p>Checken of WA plus met het zelfde voertuig en policyholder 20% duurder is dan de normale WA.</b>
<h2>Technieken</h2>
<p>Ik verwacht 1 specifieke uitkomst, dus er kan een fact gebruikt worden, eenmalig goed weet ik dat de berekening klopt.</p>
<h2>Gebruikte data:</h2>
<p>Het is een fact, en vervolgens een vergelijking. Dus aan de hand van dezelfde Vehicle en policyHolder check ik alle 2 de verzekeringen.</p>
<h2>Geteste Factor:</h2>
<p>Is de WAPlus 20% duurder dan de normale WA?</p>
<h2>Verwachting:</h2>
<p>Ik verwacht dat alles hier goed gaat, alles lijkt in orde in de code.</p>
<h2>Ondervinding:</h2>
<p>
De berekening werkt naar verwachting!
</p>
<hr>
<h2>Test 7: Is <b>All Risk</b> 2 keer zo duur?</h2>
<h2>Doel</h2>
<p>Checken of All Risk met het zelfde voertuig en policyholder 2x zo duur is als WA</b>
<h2>Technieken</h2>
<p>Ik verwacht 1 specifieke uitkomst, dus er kan een fact gebruikt worden, eenmalig goed weet ik dat de berekening klopt.</p>
<h2>Gebruikte data:</h2>
<p>Het is een fact, en vervolgens een vergelijking. Dus aan de hand van dezelfde Vehicle en policyHolder check ik alle 2 de verzekeringen.</p>
<h2>Geteste Factor:</h2>
<p>Is de All risk 2x duurder dan de normale WA?</p>
<h2>Verwachting:</h2>
<p>Ik verwacht dat alles hier goed gaat, alles lijkt in orde in de code.</p>
<h2>Ondervinding:</h2>
<p>
De berekening werkt naar verwachting!
</p>
<hr>
<h2>Test 8: Krijg je de juiste kortingen voor shadevrije jaren?</h2>
<h2>Doel</h2>
<p>Checken of je na 6 schadevrije jaren opeen tellend 5% korting krijgt tot max 65%.</b>
<h2>Technieken</h2>
<p>Voor deze test gebruik ik een theory waar ik al mijn bedachte grenswaarden/equivilentieklassen ga controlleren op hun uitkomsten.</p>
<h2>Gebruikte data:</h2>

    [Theory]
    [InlineData(5, 22.94)]
    [InlineData(6, 21.8)]
    [InlineData(12, 14.91)]
    [InlineData(18, 8.03)]
    [InlineData(24, 8.03)]
<h2>Geteste Factor:</h2>
<p>Krijg ik meer korting nadat ik vijf jaar schadevrij heb gereden?</p>
<p>Is de korting oplopend tot max 65%?</p>
<h2>Verwachting:</h2>
<p>Ik verwacht dat alles hier goed gaat, omdat ik een paar testen eerder als zag dat er hier met een Int gewerkt werd inplaats van een Double. Oftewel ik denk dat het enige probleem eerder al inzag. Omdat in de requirements stond dat je moest kunnen afronden klopten die Int in mijn hoofd al niet.</p>
<h2>Ondervinding:</h2>
<p>
De berekening werkt dus inderdaad naar verwachting!
</p>


# Overige informatie
## Algemene evervaring
<div>
<p>
Na jullie uitleg van stryker te hebben gezien en het doornemen van de requirements, besloot ik direct een raport uit te draaien en begreep er nog steeds niet zoveel van, dus besloot ik maar gewoon bij het begin te beginnen en de app te starten.
Al snel bedachte ik mij een hoop dingen zoals een string in vullen waar om een nummer werdt gevraagd, onzin in vullen waar om een datum werd gevraagd en de app crashed per direct vanwage Int, Date parse enzovoorts. Vanuit daar besloot ik de conslusie te trekken dat ik niet de hele app wil herschrijven in die 1,5 week die hebben, dus ik las de requirements nog is door en er werdt gelukkig niet gevraagd voor dat allemaal.
</p>
<p>
Toen ik uiteindelijk een aantal tests had geschreven tegen premiunCalculation aan, en vervolgens weer een raport uitdraaiden had ik toch goed een goeie 93% voor premiunCalculation en besloot even een oog te werpen op Vehicle.cs en PolicyHolder.cs. Ik begreep totaal niet wat stryker bedoelde met hun Survived, killed en No coverage, dus besloot ik maar gewoon te googlen, na een <b>HELE</b> hoop searches verder en een <b>HELE</b> hoop raporten verder werdt het eindelijk duidelijk en heb ik de foute in de code weten op te lossen. Waardoor Vehicle.cs en PolicyHolder.cs <b>100%</b> zijn geworden! Voor zo ver ik weet hoefde Program.cs niet getest te worden, zou geen idee hebben hoe namelijk.
</p>
</div>

## Algemene issues
Maar was niet van plan de hele app te herschrijven.
<ul>
<li>Er zou eigenlijk een check moeten zijn voor 18 jaar of ouder.</li>
<li>Waardes moeten gechecked worden voor ze geparsed worden.</li>
<li>Datum moeten gechecked worden op legitieme datums.</li>
</ul>


